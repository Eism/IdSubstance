#include "client.h"

#include <QDataStream>
#include <QTime>

Client::Client(const QString &strHost, int nPort, QObject *parent)
    : QObject(parent), m_nNextBlockSize(0)
{
    m_pTcpSocket = new QTcpSocket(this);

    m_pTcpSocket->connectToHost(strHost, nPort);
    connect(m_pTcpSocket, SIGNAL(connected()), SLOT(slotConnected()));
    connect(m_pTcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(slotError(QAbstractSocket::SocketError)));

    file = new QFile("log.txt");

    if (!file->open(QIODevice::Append | QIODevice::Text))
        return;

}

void Client::slotReadyRead()
{
    QString readCurData = m_pTcpSocket->readAll();

    QTextStream out(file);

    out << "[" << QTime::currentTime().toString(QString("hh:mm:ss")) << "]"
        << "Info: " << "Get bytes: <" << readCurData << ">\n";
    int value = readCurData.replace("\r\n","").toInt();

    //qDebug() << value;

    out << "[" << QTime::currentTime().toString(QString("hh:mm:ss")) << "]"
        << "Info: " << "Get " << value << "\n";
    emit setCurrentValue(value);
}

void Client::slotError(QAbstractSocket::SocketError err)
{
    QString strError =
            "Error: " + (err == QAbstractSocket::HostNotFoundError
                         ? "The host was not found."
                         : err == QAbstractSocket::RemoteHostClosedError
                           ? "The remote host is closed." :
                             err == QAbstractSocket::ConnectionRefusedError ?
                                 "The connection was refused." :
                                 QString(m_pTcpSocket->errorString()));
    qDebug() << strError;
    QTextStream out(file);
    out << "[" << QTime::currentTime().toString(QString("hh:mm:ss")) << "] "
        << strError << "\n";
}

void Client::sendMessage(QString msg)
{
    if (m_pTcpSocket->isOpen())
        m_pTcpSocket->write(msg.toUtf8());
}

void Client::slotConnected()
{
    qDebug() << "Received the connected() signal";
    QTextStream out(file);
    out << "[" << QTime::currentTime().toString(QString("hh:mm:ss")) << "] "
        << "Info: " << "Received the connected() signal" << "\n";
}
