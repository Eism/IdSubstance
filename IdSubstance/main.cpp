#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QObject>
#include <QQmlContext>

#include "client.h"
#include "data.h"
#include "dataviewmodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    Client* client = new Client("192.168.4.1", 8888);   // откуда получаем данные
    Client* listener = new Client("192.168.4.1", 5555); // куда отправляем данные

    Data* data = new Data();
    data->setListener(listener);

    QObject::connect(client, SIGNAL(setCurrentValue(int)), data, SLOT(setCurrentValue(int)));

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("modelData", new DataViewModel(data));
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
