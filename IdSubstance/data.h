#ifndef DATA_H
#define DATA_H

#include "client.h"

#include <QObject>
#include <QIODevice>

class Data : public QObject
{
    Q_OBJECT
public:
    explicit Data(QObject *parent = nullptr);

    QString getCurrentName();

    int getAir() const;
    void setAir(int value);

    int getOil() const;
    void setOil(int value);

    int getWater() const;
    void setWater(int value);

    int getCurrentValue() const;

    void setListener(Client *_listener);

private:
    void initValues();

signals:
    void changeCurrentValue(int value);

public slots:
    void setCurrentValue(int value);

private slots:
    void sendToListner();

private:
    int air;
    int oil;
    int water;
    int currentValue;
    int currentValueNum;

    Client *listener;
};

#endif // DATA_H
