#include "data.h"

#include <QDebug>
#include <QSettings>
#include <QTimer>

Data::Data(QObject *parent) : QObject(parent)
{
    initValues();

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(sendToListner()));
    timer->start(1000);
}

QString Data::getCurrentName()
{
    if (currentValue > (air - oil)) {
        currentValueNum = 1;
        return QString("Воздух");
    }else{
        if (currentValue <= (air - oil)
                && currentValue > (air - water)) {
            currentValueNum = 2;
            return QString("Масло");
        }else{
            currentValueNum = 3;
            return QString("Вода");
        }
    }
}

void Data::initValues()
{
    QSettings *settings = new QSettings("settings.ini", QSettings::IniFormat);
    settings->beginGroup("values");

    air = settings->value("air", 10).toInt();
    oil = settings->value("oil", 2).toInt();
    water = settings->value("water", 5).toInt();

    settings->endGroup();
}

void Data::setCurrentValue(int value)
{
    currentValue = value;
    emit changeCurrentValue(currentValue);
}

void Data::sendToListner()
{
    listener->sendMessage(QString::number(currentValue) + " " + QString::number(currentValueNum) + "\r\n");
}

int Data::getCurrentValue() const
{
    return currentValue;
}

void Data::setListener(Client *_listener)
{
    listener = _listener;
}

int Data::getWater() const
{
    return water;
}

void Data::setWater(int value)
{
    water = value;
}

int Data::getOil() const
{
    return oil;
}

void Data::setOil(int value)
{
    oil = value;
}

int Data::getAir() const
{
    return air;
}

void Data::setAir(int value)
{
    air = value;
}
