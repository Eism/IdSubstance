import QtQuick 2.9
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3

Item {
    id: item1
    property alias waterTextField: waterTextField
    width: 500
    height: 250
    property alias buttonSaveAir: buttonSaveAir

    Pane {
        id: pane
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.left: column.right
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 20

        Material.elevation: 6

        background: Rectangle {
            color: "#666666"
            anchors.fill: parent
        }

        //Material.color:
        Text {
            id: currentName
            text: modelData.currentName
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.preferredHeight: 26
            Layout.preferredWidth: 52
            font.pixelSize: 40
            color: "#ffffff"
        }
    }

    RowLayout {
    }

    ColumnLayout {
        id: column
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20

        GridLayout {
            columnSpacing: 8
            rowSpacing: 8
            Layout.columnSpan: 2
            Layout.rowSpan: 3
            rows: 3
            columns: 3

            Text {
                id: airLabel
                text: qsTr("Воздух")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 14
            }

            TextField {
                id: airTextField
                horizontalAlignment: Text.AlignHCenter
                text: modelData.air
                onTextChanged: modelData.air = text
            }

            Button {
                id: buttonSaveAir
                text: qsTr("Save")
            }

            Text {
                id: oilLabel
                text: qsTr("Масло")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 14
            }

            TextField {
                id: oilTextField
                text: modelData.oil
                horizontalAlignment: Text.AlignHCenter
                onTextChanged: modelData.oil = text
            }

            Item {
                id: spacer
                Layout.preferredHeight: 14
                Layout.preferredWidth: 14
            }

            Text {
                id: waterLabel
                text: qsTr("Вода")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 14
            }

            TextField {
                id: waterTextField
                horizontalAlignment: Text.AlignHCenter
                text: modelData.water
                onTextChanged: modelData.water = text
            }

            Item {
                id: spacer1
                Layout.preferredHeight: 14
                Layout.preferredWidth: 14
            }
        }

        RowLayout {

            Text {
                id: currentLabel
                text: qsTr("Текущее")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                Layout.preferredHeight: 53
                Layout.preferredWidth: 93
                font.pixelSize: 14
            }

            Text {
                id: currentText
                text: modelData.current
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 14
            }
        }
    }
}
