#ifndef DATAVIEWMODEL_H
#define DATAVIEWMODEL_H

#include <QObject>

#include "data.h"

class DataViewModel : public QObject
{
    Q_OBJECT
public:
    explicit DataViewModel(Data *_data, QObject *parent = nullptr);

    Q_PROPERTY(int air READ getAir WRITE setAir NOTIFY changeAir)
    Q_PROPERTY(int oil READ getOil WRITE setOil NOTIFY changeOil)
    Q_PROPERTY(int water READ getWater WRITE setWater NOTIFY changeWater)
    Q_PROPERTY(int current READ getCurrent NOTIFY changeCurrent)
    Q_PROPERTY(QString currentName READ getCurrentName NOTIFY changeCurrentName)

    Q_INVOKABLE void saveZeroOffset();

    int getAir();
    void setAir(const int value);

    int getOil();
    void setOil(const int value);

    int getWater();
    void setWater(const int value);

    int getCurrent();

    QString getCurrentName();

signals:
    void changeAir();
    void changeOil();
    void changeWater();
    void changeCurrent();
    void changeCurrentName();

public slots:

private:
    Data* data;
};

#endif // DATAVIEWMODEL_H
