#include "dataviewmodel.h"

#include <QDebug>

DataViewModel::DataViewModel(Data *_data, QObject *parent) : QObject(parent)
{
    data = qMove(_data);
    connect(data, &Data::changeCurrentValue, [=](){
       emit changeCurrent();
       emit changeCurrentName();
    });
}

void DataViewModel::saveZeroOffset()
{
    //qDebug() << "set" << data->getCurrentValue();
    data->setAir(data->getCurrentValue());
    emit changeAir();
}

int DataViewModel::getAir()
{
    return data->getAir();
}

void DataViewModel::setAir(const int value)
{
    if (data->getAir() == value)
        return;
    data->setAir(value);
    emit changeAir();
}

int DataViewModel::getOil()
{
    return data->getOil();
}

void DataViewModel::setOil(const int value)
{
    if (data->getOil() == value)
        return;

    data->setOil(value);
    emit changeOil();
}

int DataViewModel::getWater()
{
    return data->getWater();
}

void DataViewModel::setWater(const int value)
{
    if (data->getWater() == value)
        return;

    data->setWater(value);
    emit changeWater();
}

int DataViewModel::getCurrent()
{
    return data->getCurrentValue();
}

QString DataViewModel::getCurrentName()
{
    return data->getCurrentName();
}
