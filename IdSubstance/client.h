#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QFile>

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(const QString& strHost, int nPort, QObject *parent = nullptr);

    void sendMessage(QString msg);

private:
    QTcpSocket* m_pTcpSocket;
    quint16 m_nNextBlockSize;

signals:
    void setCurrentValue(int value);

private slots:
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError err);
    void slotConnected();

private:
    QFile* file;
};

#endif // CLIENT_H
